"""module __main__
"""

import datetime

from project.centre_secours.caserne import Caserne
from project.centre_secours.ressource import DataFrameRessource
from project.data_frame.data_frame_caserne import DataFrameCaserne
from project.data_frame.data_frame_urgence import DataFrameUrgence
from project.declaration_urgence.urgence import Urgence
from project.tableau_de_bord.tableau_de_bord_caserne import TableauDeBordCaserne
from project.tableau_de_bord.tableau_de_bord_ressource import TableauDeBordRessource
from project.tableau_de_bord.tableau_de_bord_urgence import TableauDeBordUrgence

# Tout d'abord, on charge les bases de données:
bdd_caserne = DataFrameCaserne.from_csv("data/bdd_casernes_montgomery_county.csv")
bdd_ressources = DataFrameRessource.from_csv("data/bdd_ressources.csv")
bdd_urgences = DataFrameUrgence.from_csv("data/bdd_urgence.csv")

# Dans un premier temps, on se place dans le rôle d'une caserne et on souhaite faire
# quelques manipulations sur les casernes.
# Pour cela, il nous faut d'abord récupérer l'ensemble des casernes présentes sur la
# base de données:
dict_casernes = Caserne.creer_dict_caserne(
    dataframe_caserne=bdd_caserne, dataframe_ressources=bdd_ressources
)

# Ainsi, nous pouvons accéder à toutes les casernes. Disons que nous sommes la caserne
# n°41, voici le résumé de notre caserne:
print("Notre caserne : \n")
print(dict_casernes[41])


# Malheureusement, notre ressource A41E vient de tomber en panne!:
dict_casernes[41].changer_statut_ressource("A41E", "hors-service")
print("\nNotre caserne après le changement de statut de la ressource A41E: \n")
print(dict_casernes[41])


# A présent, on souhaite céder notre ressource RS741 à la caserne n°20 qui ne possède
# pas de ressource de type Rescue Squad:
print("\nCaserne n°20 avant d'acquérir la nouvelle ressource:\n")
print(dict_casernes[20])
dict_casernes[41].ceder_ressource(dict_casernes[20], "RS741")
print("\nLa caserne n°20 après que la caserne n°41 lui cède sa ressource RS741:\n")
print(dict_casernes[20])
print("\nNotre caserne n°41 après avoir cédé notre ressource RS741:\n")
print(dict_casernes[41])
tdb_caserne41 = TableauDeBordRessource(
    bdd_ressources, bdd_caserne, "nombre", filtre_caserne=[41]
)
print(tdb_caserne41)


# A present, nous sommes dans le rôle du responsable du comté. Nous souhaitons afficher
# la liste des ressource qui sont en service en ce moment:
tdb = TableauDeBordRessource(
    dataframe_ressources=bdd_ressources,
    dataframe_casernes=bdd_caserne,
    info_demandee="liste",
    filtre_statut=["en service"],
)
print("\nLa liste des ressources en service pour l'instant:")
print(tdb)
# Il n'y a aucune ressource en service... Journée plutôt calme !
# Ou alors il y a des urgences en cours mais aucune ressource n'a été envoyée !?
tdb_urgence = TableauDeBordUrgence(
    bdd_urgences, info_demandee="nombre", filtre_etat=["en cours"]
)
print("\nY a-t-il des urgences en cours?")
print(tdb_urgence)
# Non c'est bon, aucune urgence en cours.

# Nous venons de recevoir un appel: un python a été apperçu dans une maison !!!
# Nous prenons donc maintenant le rôle du centre d'appel pour déclarer l'urgence:
Urgence.creer_urgence(
    id_urgence=20,
    dataframe_urgences=bdd_urgences,
    dataframe_casernes=bdd_caserne,
    dataframe_ressources=bdd_ressources,
    date="2024-04-03",
    emplacement=(39.283543, -77.192288),
    ville="Damascus",
    zip_code="20872",
    nature_urgence="un python dans la maison",
    auteur_appel_nom="Gandolfi",
    auteur_appel_prenom="Hermione",
    auteur_appel_num_tel="444-444-444",
    liste_ressources={"Special Unit": 2},
)

# Le responsable peut donc remarquer un changement sur son tableau de bord:
tdb2 = TableauDeBordRessource(
    dataframe_ressources=bdd_ressources,
    dataframe_casernes=bdd_caserne,
    info_demandee="liste",
    filtre_statut=["en service"],
)
tdb_urgence2 = TableauDeBordUrgence(
    bdd_urgences, info_demandee="nombre", filtre_etat=["en cours"]
)
print("\nLa liste des ressources en service après la déclaration de l'urgence:")
print(tdb2)
print("\nEn effet, le nombre d'urgence en cours a augmenté:")
print(tdb_urgence2)
# En effet, les ressources BUTV709 et SU729 ont été requisitionnées pour intervenir sur
# l'urgence.


# Cette urgence ressemble étrangement à une autre urgence déclarée récemment...
tdb_urgence_python = TableauDeBordUrgence(
    bdd_urgences,
    info_demandee="liste",
    filtre_date=(datetime.date(2024, 4, 1), datetime.date(2024, 4, 3)),
    filtre_nature=["un python dans la maison"],
)
print("\nVoyons les urgences impliquant un python dans une maison:")
print(tdb_urgence_python)
# En effet, l'urgence n°3 concernait déjà un python dans une maison. Et maintenant,
# l'urgence d'aujourd'hui, la n°7, concerne également un python dans une maison.
# Etrange...


# Heureusement, l'urgence en cours vient de prendre fin:
Urgence.finir_urgence(20, bdd_urgences, bdd_ressources)

# On peut ainsi vérifier le nombre d'urgence en cours et le nombre de ressources en
# service
tdb_nb_urgence_fin = TableauDeBordUrgence(
    bdd_urgences, info_demandee="nombre", filtre_etat=["en cours"]
)
tdb_nb_ressource_en_service_fin = TableauDeBordRessource(
    bdd_ressources, bdd_caserne, info_demandee="nombre", filtre_statut=["en service"]
)
print("\nSuite à la fin de l'urgence:")
print(tdb_nb_urgence_fin, tdb_nb_ressource_en_service_fin)


# En tant que responsable, nous sommes inquiets: y a-t-il assez de casernes dans chaque
# ville ?
tdb_caserne = TableauDeBordCaserne(bdd_caserne, info_demandee="moyenne")
print("\nY a-t-il assez de casernes dans chaque ville?")
print(tdb_caserne)


# De plus: y a-t-il assez de ressources dans chaque caserne ?
tdb_moyenne_nb_ressources = TableauDeBordRessource(
    bdd_ressources,
    bdd_caserne,
    info_demandee="moyenne",
    filtre_pour_moyenne="par caserne",
)
print("\nY a-t-il assez de ressources dans chaque caserne?")
print(tdb_moyenne_nb_ressources)


# Enfin, on souhaite connaitre le nombre moyen d'urgences par jour dans les villes
# principales du comté pour le mois de janvier 2023:
tdb_urgence_janvier2023 = TableauDeBordUrgence(
    bdd_urgences,
    info_demandee="moyenne",
    filtre_pour_moyenne="par jour",
    filtre_date=(datetime.date(2023, 1, 1), datetime.date(2023, 1, 31)),
    filtre_ville=[
        "Germantown",
        "Poolesville",
        "Gaithersburg",
        "Damascus",
        "Rockville",
        "Potomac",
        "Bethesda",
        "Olney",
        "Clarksburg",
    ],
)
print("\nPour les villes principales du comtés:")
print(tdb_urgence_janvier2023)


# Afin de pouvoir réutiliser plusieurs fois le même fichier __main__ sans avoir d'erreur
# du type "L'urgence n°20 existe déjà.", on ne sauvegarde pas les dataframes au format
# csv. Cependant, cela est fait normalement avec les commandes suivantes:

# bdd_urgences.save_csv("data/bdd_urgence.csv")
# bdd_caserne.save_csv("data/bdd_casernes_montgomery_county.csv")
# bdd_ressources.save_csv("data/bdd_ressources.csv")

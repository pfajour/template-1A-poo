"""Implémentation de la classe Urgence
"""

import datetime as dt
import warnings

import pandas as pd
from geopy.distance import geodesic

from ..centre_secours.caserne import Caserne
from ..centre_secours.ressource import DataFrameRessource, Ressource
from ..data_frame.data_frame_caserne import DataFrameCaserne
from ..data_frame.data_frame_urgence import DataFrameUrgence
from ..ville.ville import Ville
from .auteur_appel import AuteurAppel


class Urgence:
    """Classe Urgence"""

    def __init__(
        self,
        id_urgence: int,
        dataframe_urgences: DataFrameUrgence,
        dataframe_casernes: DataFrameCaserne,
        dataframe_ressources: DataFrameRessource,
        date: dt.date,
        emplacement: tuple,
        ville: str,
        zip_code: str,
        nature_urgence: str,
        auteur_appel_nom: str,
        auteur_appel_prenom: str,
        auteur_appel_num_tel: str,
        liste_ressources: dict,
    ) -> None:
        # On vérifie le type des dataframe
        if not isinstance(dataframe_urgences, DataFrameUrgence):
            raise TypeError(
                "Le dataframe des urgences doit être de type DataFrameUrgence."
            )
        if not isinstance(dataframe_casernes, DataFrameCaserne):
            raise TypeError(
                "Le dataframe des casernes doit être de type DataFrameCaserne."
            )
        if not isinstance(dataframe_ressources, DataFrameRessource):
            raise TypeError(
                "Le dataframe des ressources doit être de type DataFrameRessource."
            )

        # on vérifie le type des autres arguments
        if not isinstance(id_urgence, int):
            raise TypeError("l'id de l'urgence doit être un entier.")
        if not isinstance(date, dt.date):
            raise TypeError("La date doit être de type datetime.date .")
        if not isinstance(ville, str):
            raise TypeError("La ville doit être une chaîne de caractères.")
        if not isinstance(zip_code, str):
            raise TypeError("Le zip code doit être une chaîne de caractères.")
        if not isinstance(nature_urgence, str):
            raise TypeError(
                "La nature de l'urgence doit être sous forme de chaîne de "
                "caractères."
            )
        if not isinstance(liste_ressources, dict):
            raise TypeError(
                "La liste de ressources doit être sous la forme d'un dictionnaire."
            )
        if not isinstance(emplacement, tuple):
            raise TypeError("L'emplacement doit être un tuple de 2 réels.")

        # On vérifie le format de l'emplacement
        if len(emplacement) != 2:
            raise ValueError(
                "L'emplacement de l'urgence n'est pas valide (tuple de 2 floats)."
            )
        for element in emplacement:
            if not isinstance(element, float):
                raise ValueError(
                    "L'emplacement de l'urgence n'est pas valide (tuple "
                    "de 2 floats)."
                )

        # On vérifie le format de liste_ressources
        for cles in liste_ressources.keys():
            if cles not in [
                "Truck",
                "Engine",
                "Medic",
                "Ambulance",
                "Rescue Squad",
                "Tanker",
                "Special Unit",
            ]:
                raise ValueError(
                    "Les cles du dictionnaire de la listes des ressources"
                    " ne correspondent pas aux types de ressource "
                    "possibles."
                )
        for nombre in liste_ressources.values():
            if not isinstance(nombre, int):
                raise ValueError(
                    "Le nombre de ressource nécessaire doit être un entier positif."
                )
            if nombre <= 0:
                raise ValueError(
                    "Le nombre de ressource nécessaire doit être un entier positif."
                )

        # On initialise les dataframes
        self.dataframes = {
            "urgence": dataframe_urgences,
            "caserne": dataframe_casernes,
            "ressources": dataframe_ressources,
        }

        # On initialise la date et les variables relatives au lieu
        self.date = date
        self.emplacement = emplacement
        self.ville = Ville(ville, zip_code)

        # On initialise les variables relatives à l'urgence en elle-même
        self.nature_urgence = nature_urgence
        self.liste_ressources = self.__trouver_ressources(
            liste_ressources, emplacement, dataframe_casernes, dataframe_ressources
        )
        self.etat_urgence = "en cours"
        self.id_urgence = id_urgence

        # On initialise l'auteur de l'appel
        id_auteur_appel = max(dataframe_urgences.df["id_auteur_appel"].to_list()) + 1
        self.auteur_appel = AuteurAppel(
            id_auteur_appel, auteur_appel_nom, auteur_appel_prenom, auteur_appel_num_tel
        )

        # On ajoute l'urgence au dataframe des urgences
        liste_ressources_str = ""
        for ressource in self.liste_ressources:
            liste_ressources_str += ressource.id_ressource + ","
        liste_ressources_str = liste_ressources_str.rstrip(",")

        nouvelle_urgence_df = DataFrameUrgence(
            [
                {
                    "id_urgence": self.id_urgence,
                    "date": self.date.strftime("%Y-%m-%d"),
                    "latitude": self.emplacement[0],
                    "longitude": self.emplacement[1],
                    "ville": self.ville.nom,
                    "zip_code": self.ville.zip_code,
                    "nature_urgence": self.nature_urgence,
                    "id_auteur_appel": self.auteur_appel.id_auteur_appel,
                    "auteur_appel_nom": self.auteur_appel.nom,
                    "auteur_appel_prenom": self.auteur_appel.prenom,
                    "auteur_appel_num_tel": self.auteur_appel.num_tel,
                    "liste_ressources": liste_ressources_str,
                    "etat_urgence": self.etat_urgence,
                }
            ]
        )

        dataframe_urgences.df = pd.concat(
            [dataframe_urgences.df, nouvelle_urgence_df.df], ignore_index=True
        )

    def __trouver_ressources(
        self,
        liste_ressources: dict,
        emplacement_urgence: tuple,
        dataframe_casernes: DataFrameCaserne,
        dataframe_ressources: DataFrameRessource,
    ) -> list[Ressource]:
        """méthode pour trouver les ressources disponibles nécessaires pour l'urgence
        les plus proches possible.
        """
        # On récupère la liste des casernes par ordre croissant de distance
        liste_id_caserne_triee = self.__caserne_par_ordre_distance(
            dataframe_casernes, emplacement_urgence
        )

        # on crée un dictionnaire de casernes
        dict_casernes = Caserne.creer_dict_caserne(
            dataframe_casernes, dataframe_ressources
        )

        # On charche pour chaque type de ressource les ressources dispo en parcourant
        # les casernes des plus proches aux plus éloignées
        nb_ressources_trouvees = 0
        ressources_assignees = []

        for type_ressource in liste_ressources.keys():
            n = 0
            for caserne in liste_id_caserne_triee:
                for ressource in dict_casernes[caserne].liste_ressources.values():
                    if (
                        ressource.type_ressource == type_ressource
                        and ressource.statut == "operationnel"
                        and n < liste_ressources[type_ressource]
                    ):
                        ressources_assignees.append(ressource)
                        ressource.modifier_statut("en service")
                        ressource.save_ressource()
                        nb_ressources_trouvees += 1
                        n += 1
                    if n == liste_ressources[type_ressource]:
                        break
                if n == liste_ressources[type_ressource]:
                    break

        if nb_ressources_trouvees < sum(liste_ressources.values()):
            warnings.warn(
                "Sous effectif! Le nombre de ressources disponibles n'est pas"
                " suffisent pour répondre efficacement à l'urgence. Veuillez"
                " contacter un comté voisin."
            )

        return ressources_assignees

    def __caserne_par_ordre_distance(
        self, dataframe_casernes: DataFrameCaserne, emplacement: tuple
    ) -> list[int]:
        """Méthode pour obtenir une liste des casernes par ordre croissant de distance.

        Args:
            dataframe_casernes (DataFrameCaserne): le dataframe comprenant les casernes
            emplacement (tuple): l'emplacement de l'urgence
        Return:
            list[int]: la liste des identifiants des casernes par ordre coissant de
                       distance avec l'emplacement de l'urgence
        """
        # On crée un dictionnaire regroupant la distance des différentes casernes à
        # l'urgence
        distance_caserne = {}
        for i in range(len(dataframe_casernes.df)):
            emplacement_caserne = (
                dataframe_casernes.df.at[i, "latitude"],
                dataframe_casernes.df.at[i, "longitude"],
            )
            distance_caserne[dataframe_casernes.df.at[i, "id_caserne"]] = geodesic(
                emplacement, emplacement_caserne
            ).kilometers

        # On récupère ainsi une liste d'id de caserne triée par distance croissante

        distance_caserne_tuples = list(distance_caserne.items())
        distance_caserne_tuples.sort(key=lambda x: x[1])
        liste_id_caserne_triee = [x[0] for x in distance_caserne_tuples]

        return liste_id_caserne_triee

    @classmethod
    def creer_urgence(
        cls,
        id_urgence: int,
        dataframe_urgences: DataFrameUrgence,
        dataframe_casernes: DataFrameCaserne,
        dataframe_ressources: DataFrameRessource,
        date: str,
        emplacement: tuple,
        ville: str,
        zip_code: str,
        nature_urgence: str,
        auteur_appel_nom: str,
        auteur_appel_prenom: str,
        auteur_appel_num_tel: str,
        liste_ressources: dict,
    ) -> None:
        """Méthode de classe"""
        if not isinstance(date, str):
            raise TypeError("La date doit être au format str.")

        try:
            dt.datetime.strptime(date, "%Y-%m-%d")
        except ValueError as exc:
            raise ValueError("La date doit être au fromat '%Y-%m-%d'.") from exc

        date = dt.datetime.strptime(date, "%Y-%m-%d")

        for id_urgence_existante in dataframe_urgences.df["id_urgence"].to_list():
            if id_urgence_existante == id_urgence:
                raise ValueError(f"L'urgence n°{id_urgence} existe déjà.")

        Urgence(
            id_urgence=id_urgence,
            dataframe_urgences=dataframe_urgences,
            dataframe_casernes=dataframe_casernes,
            dataframe_ressources=dataframe_ressources,
            date=date,
            emplacement=emplacement,
            ville=ville,
            zip_code=zip_code,
            nature_urgence=nature_urgence,
            auteur_appel_nom=auteur_appel_nom,
            auteur_appel_prenom=auteur_appel_prenom,
            auteur_appel_num_tel=auteur_appel_num_tel,
            liste_ressources=liste_ressources,
        )

    @classmethod
    def finir_urgence(
        cls,
        id_urgence: int,
        dataframe_urgence: DataFrameUrgence,
        dataframe_ressource: DataFrameRessource,
    ) -> None:
        """Méthode de classe permettant de déclarer la fin d'une urgence présente sur le
        dataframe des urgences.

        Args:
            id_urgence (int): L'id de l'urgence dont on veut déclarer la fin
            dataframe_urgence (DataFrameUrgence): Le dataframe qui contient l'urgence
            dataframe_ressource (DataFrameRessource): Le dataframe qui contient les
                                                      ressources
        """
        index_urgence = dataframe_urgence.df.index[
            dataframe_urgence.df["id_urgence"] == id_urgence
        ][0]
        dataframe_urgence.df.at[index_urgence, "etat_urgence"] = "finie"
        liste_id_urgence = dataframe_urgence.df.at[
            index_urgence, "liste_ressources"
        ].split(",")
        for id_ressource in liste_id_urgence:
            ressource = Ressource(id_ressource, dataframe_ressource)
            ressource.modifier_statut("operationnel")

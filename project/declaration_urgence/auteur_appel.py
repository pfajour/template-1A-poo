"""Implémentation de la classe AuteurAppel
"""


class AuteurAppel:
    """classe qui représente l'auteurd de l'appel, c'est à dire celui qui
    reporte l'urgence

    Parameters
    ----------
    id_auteur_appel : int
        identifiant de l'auteur de l'appel
    nom : str
        le nom de l'auteur de l'appel
    prenom : str
        le prénom de l'auteur de l'appel
    num_tel : str
        le numéro de téléphone de l'auteur de l'appel
    """

    def __init__(self, id_auteur_appel, nom, prenom, num_tel):
        if not isinstance(id_auteur_appel, int):
            raise TypeError("L'identifiant doit être un entier.")
        if not isinstance(nom, str):
            raise TypeError("Le nom doit être une chaîne de caractères.")
        if not isinstance(prenom, str):
            raise TypeError("Le prénom doit être une chaîne de caractères.")
        if not isinstance(num_tel, str):
            raise TypeError(
                "Le numéro de téléphone doit être une chaîne de caractères."
            )
        num_tel_liste = num_tel.split("-")
        if len(num_tel_liste) != 3:
            raise TypeError("Le format du numéro n'est pas correct.")
        for element in num_tel_liste:
            if len(element) != 3:
                raise TypeError("Le format du numéro n'est pas correct.")
            if not element.isdigit():
                raise TypeError("Le format du numéro n'est pas correct.")

        self.id_auteur_appel = id_auteur_appel
        self.nom = nom
        self.prenom = prenom
        self.num_tel = num_tel

    def __str__(self):
        return f"{self.id_auteur_appel}, {self.nom}, {self.prenom}, {self.num_tel}"

    def __eq__(self, value: object) -> bool:
        if not isinstance(value, AuteurAppel):
            raise TypeError(
                "On ne peut tester l'égalité qu'entre deux objet de la "
                "classe AuteurAppel."
            )
        return (
            self.id_auteur_appel == value.id_auteur_appel
            and self.nom == value.nom
            and self.prenom == value.prenom
            and self.num_tel == value.num_tel
        )

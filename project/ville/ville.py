"""Implémentation de la classe Ville
"""


class Ville:
    """classe qui représente les villes

    Parameters
    ----------
    nom : str
        le nom de la ville
    zip_code : str
        code postal utilisé aux États-Unis

    Examples
    --------
    >>> dawsonville = Ville('Dawsonville', '20837')
    >>> print(dawsonville)
    Dawsonville : 20837
    >>> redland = Ville('Redland', '20855')
    >>> print(redland)
    Redland : 20855
    """

    def __init__(self, nom: str, zip_code: str):
        if not isinstance(nom, str):
            raise TypeError("Le nom doit être une chaîne de caractères.")
        if not isinstance(zip_code, str):
            raise TypeError("Le zip code doit être une chaîne de caractères.")
        self.nom = nom
        self.zip_code = zip_code

    def __str__(self):
        return f"{self.nom} : {self.zip_code}"

    def __repr__(self) -> str:
        return f"Ville({self.nom}, {self.zip_code})"

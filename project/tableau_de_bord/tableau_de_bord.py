"""Implémentation de la classe TableauDeBord
"""

from abc import ABC, abstractmethod


class TableauDeBord(ABC):
    """Classe TableauDeBord"""

    def __init__(self, info_demandee: str, filtre_ville: list[str] = None) -> None:

        # On vérifie le type des arguments
        if not isinstance(info_demandee, str):
            raise TypeError("L'info demandée doit être une instance de str.")
        if filtre_ville is not None:
            if not isinstance(filtre_ville, list):
                raise TypeError(
                    "Le filtre ville doit être une liste de tuples de 2 str."
                )
            for ville in filtre_ville:
                if not isinstance(ville, str):
                    raise TypeError("Le filtre ville doit être une liste de str.")

        # On vérifie l'info demandée
        if info_demandee not in ["liste", "nombre", "moyenne"]:
            raise ValueError("L'info demandée n'est pas valide.")

        # On initialise
        self.info_demandee = info_demandee
        self.filtre_ville = filtre_ville

    @abstractmethod
    def recuperer_liste(self) -> list[str]:
        """Méthode pour récupérer une liste d'id concernés par les filtres

        Returns:
            list[str]: les id des objets concernés
        """

    def compter(self) -> int:
        """Méthode pour compter le nombre d'objets concernés par les filtres

        Returns:
            int: le nombre d'objets concernés par les filtres
        """
        return len(self.recuperer_liste())

    @abstractmethod
    def moyenne(self) -> float:
        """Méthode pour calculer une moyenne en fonction du filtre pour la moyenne

        Returns:
            float: la moyenne
        """

    def titre_affichage(self) -> str:
        """Méthode pour créer le titre du tableau de bord qui sera utilisé pour
        l'affichage (méthode __str__)

        Returns:
            str: le titre pour l'affichage
        """

    def __str__(self) -> str:
        if self.info_demandee == "liste":
            affichage = self.titre_affichage() + str(self.recuperer_liste())
        if self.info_demandee == "nombre":
            affichage = self.titre_affichage() + str(self.compter())
        if self.info_demandee == "moyenne":
            affichage = self.titre_affichage() + str(self.moyenne())

        return "\n" + affichage + "\n"

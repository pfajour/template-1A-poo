"""Implémentation de la classe TableauDeBordUrgence héritée de la classe TableauDeBord.
"""

import datetime as dt

from dateutil.relativedelta import relativedelta

from ..data_frame.data_frame_urgence import DataFrameUrgence
from .tableau_de_bord import TableauDeBord


class TableauDeBordUrgence(TableauDeBord):
    """classe TableauDeBordUrgence héritée de la classe TableauDeBord"""

    def __init__(
        self,
        data_frame: DataFrameUrgence,
        info_demandee: str,
        filtre_pour_moyenne: str = None,
        filtre_ville: list[str] = None,
        filtre_etat: list[str] = None,
        filtre_date: tuple = None,
        filtre_nature: list[str] = None,
    ) -> None:

        super().__init__(info_demandee, filtre_ville)

        # On traite filtre par filtre

        # filtre etat
        if filtre_etat is not None:
            if not isinstance(filtre_etat, list):
                raise TypeError("Le filtre etat doit être une liste de str.")
            for etat in filtre_etat:
                if not isinstance(etat, str):
                    raise TypeError("Le filtre etat doit être une liste de str.")
                if etat not in ["en cours", "finie"]:
                    raise ValueError("L'etat doit être soit 'en cours', soit 'finie'.")

        # filtre date
        if filtre_date is not None:
            if not isinstance(filtre_date, tuple):
                raise TypeError(
                    "Le filtre date doit être un tuple de 2 date au format "
                    "datetime.date."
                )
            if len(filtre_date) != 2:
                raise TypeError(
                    "Le filtre date doit être un tuple de 2 date au format "
                    "datetime.date."
                )
            if not (
                isinstance(filtre_date[0], dt.date)
                and isinstance(filtre_date[1], dt.date)
            ):
                raise TypeError(
                    "Le filtre date doit être un tuple de 2 date au format "
                    "datetime.date."
                )

        # filtre nature

        if filtre_nature is not None:
            if not isinstance(filtre_nature, list):
                raise TypeError("Le filtre nature doit être une liste de str.")
            for element in filtre_nature:
                if not isinstance(element, str):
                    raise TypeError("Le filtre nature doit être une liste de str.")

        # filtre pour moyenne

        if filtre_pour_moyenne is not None:
            if not isinstance(filtre_pour_moyenne, str):
                raise TypeError(
                    "Le filtre pour moyenne doit être une chaîne de caractères."
                )
            if filtre_pour_moyenne not in [
                "par ville",
                "par jour",
                "par mois",
                "par année",
            ]:
                raise ValueError("Le filtre pour moyenne n'est pas valide.")

        # Si l'info demandee est une moyenne, le filtre pour moyenne ne peut pas être
        # None, et inversement:

        if info_demandee == "moyenne" and filtre_pour_moyenne is None:
            raise ValueError("Veuillez renseigner un filtre pour la moyenne.")
        if info_demandee != "moyenne" and filtre_pour_moyenne is not None:
            raise ValueError(
                "On ne peut pas renseigner un filtre pour la moyenne si "
                "l'info demandée n'est pas une moyenne."
            )

        # data_frame_urgence
        if not isinstance(data_frame, DataFrameUrgence):
            raise TypeError("Le dataframe urgence doit être de type DataFrameUrgence.")

        # On initialise
        self.data_frame = data_frame
        self.filtre_pour_moyenne = filtre_pour_moyenne
        self.filtre_etat = filtre_etat
        self.filtre_date = filtre_date
        self.filtre_nature = filtre_nature

    def recuperer_liste(self) -> list[str]:
        """Méthode permettant de récupérer la liste des urgences concernées par les
        filtres.

        Returns:
            list[str]: liste des id des urgences concernées par les filtres
        """

        urgences_concernees = []

        if self.filtre_ville is not None:
            urgences_concernees_ville = []
            for ville in self.filtre_ville:
                urgences_concernees_ville.extend(
                    self.data_frame.df[
                        (self.data_frame.df["ville"] == ville)
                        | (self.data_frame.df["zip_code"] == ville)
                    ]["id_urgence"].to_list()
                )
            urgences_concernees.append(urgences_concernees_ville)

        if self.filtre_etat is not None:
            urgences_concernees_etat = []
            for etat in self.filtre_etat:
                urgences_concernees_etat.extend(
                    self.data_frame.df[self.data_frame.df["etat_urgence"] == etat][
                        "id_urgence"
                    ].to_list()
                )
            urgences_concernees.append(urgences_concernees_etat)

        if self.filtre_nature is not None:
            urgences_concernees_nature = []
            for nature in self.filtre_nature:
                urgences_concernees_nature.extend(
                    self.data_frame.df[self.data_frame.df["nature_urgence"] == nature][
                        "id_urgence"
                    ].to_list()
                )
            urgences_concernees.append(urgences_concernees_nature)

        if self.filtre_date is not None:
            urgences_concernees_date = []
            urgences_concernees_date.extend(
                self.data_frame.df[
                    (self.data_frame.df["date"] >= self.filtre_date[0])
                    & (self.data_frame.df["date"] <= self.filtre_date[1])
                ]["id_urgence"].to_list()
            )
            urgences_concernees.append(urgences_concernees_date)

        urgences_concernees_finale = self.data_frame.df["id_urgence"].to_list()
        for element in urgences_concernees:
            urgences_concernees_finale = list(
                set(urgences_concernees_finale) & set(element)
            )

        return urgences_concernees_finale

    def compter(self) -> int:
        """Méthode pour compter le nombre d'urgences si l'info demandée est 'nombre'

        Returns:
            int: le nombre d'urgences
        """
        return len(self.recuperer_liste())

    def moyenne(self) -> float:
        """Méthode pour calculer la moyenne si l'info demandée est 'moyenne'

        Returns:
            float: la moyenne
        """
        if self.filtre_pour_moyenne == "par ville":
            if self.filtre_ville is not None:
                moyenne = self.compter() / len(self.filtre_ville)
            else:
                denom = self.data_frame.df["ville"].nunique()
                moyenne = self.compter() / denom
        if self.filtre_pour_moyenne == "par jour":
            moyenne = self.compter() / (self.filtre_date[1] - self.filtre_date[0]).days
        if self.filtre_pour_moyenne == "par mois":
            dif_duree = relativedelta(self.filtre_date[1], self.filtre_date[0])
            dif_en_mois = dif_duree.months + dif_duree.years * 12
            moyenne = self.compter() / dif_en_mois
        if self.filtre_pour_moyenne == "par année":
            dif_duree = relativedelta(self.filtre_date[1], self.filtre_date[0])
            dif_en_annee = dif_duree.years
            moyenne = self.compter() / dif_en_annee

        return round(moyenne, 2)

    def titre_affichage(self) -> str:
        """Méthode pour créer le titre du tableau de bord qui sera utilisé pour
        l'affichage (méthode __str__)

        Returns:
            str: le titre pour l'affichage
        """
        if self.info_demandee == "moyenne":
            titre_affichage = (
                f"moyenne du nombre d'urgence {self.filtre_pour_moyenne}: \n"
            )
        else:
            titre_affichage = f"{self.info_demandee} urgences: \n"

        if self.filtre_date is not None:
            titre_affichage += (
                f"Entre: {self.filtre_date[0]} et {self.filtre_date[1]}\n"
            )

        if self.filtre_ville is not None:
            villes_str = ""
            for ville in self.filtre_ville:
                villes_str += ville + ", "
            villes_str = villes_str.rstrip(", ")
            titre_affichage += f"Dans les villes: {villes_str}\n"

        if self.filtre_nature is not None:
            titre_affichage += f"De nature: {', '.join(self.filtre_nature)}\n"

        if self.filtre_etat is not None:
            titre_affichage += f"Etat de l'urgence: {', '.join(self.filtre_etat)}\n"

        return titre_affichage

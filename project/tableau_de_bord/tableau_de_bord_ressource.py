"""Classe TableauDeBordRessource
"""

from ..data_frame.data_frame_caserne import DataFrameCaserne
from ..data_frame.data_frame_ressource import DataFrameRessource
from .tableau_de_bord import TableauDeBord
from .tableau_de_bord_caserne import TableauDeBordCaserne


class TableauDeBordRessource(TableauDeBord):
    """Classe TableauDeBordRessource"""

    def __init__(
        self,
        dataframe_ressources: DataFrameRessource,
        dataframe_casernes: DataFrameCaserne,
        info_demandee: str,
        filtre_pour_moyenne: str = None,
        filtre_ville: list[str] = None,
        filtre_caserne: list[int] = None,
        filtre_type: list[str] = None,
        filtre_statut: list[str] = None,
    ) -> None:

        super().__init__(info_demandee, filtre_ville)

        # filtre par filtre

        # filtre pour moyenne
        if info_demandee == "moyenne" and filtre_pour_moyenne is None:
            raise ValueError(
                "Si l'info demandée est la moyenne, il faut renseigner "
                "un filtre pour la moyenne."
            )

        if info_demandee != "moyenne" and filtre_pour_moyenne is not None:
            raise ValueError(
                "Si l'info demandée n'est pas une moyenne, il ne faut pas "
                "renseigner de filtre pour la moyenne."
            )

        if filtre_pour_moyenne is not None:
            if not isinstance(filtre_pour_moyenne, str):
                raise TypeError("Le filtre pour la moyenne doit être de type str.")
            if filtre_pour_moyenne not in ["par ville", "par caserne"]:
                raise ValueError("Le filtre pour moyenne n'est pas valide.")

        # filtre caserne
        if filtre_caserne is not None:
            if not isinstance(filtre_caserne, list):
                raise TypeError("Le filtre caserne doit être une liste d'entiers.")
            for caserne in filtre_caserne:
                if not isinstance(caserne, int):
                    raise TypeError("Le filtre caserne doit être une liste d'entiers.")

        # filtre type
        if filtre_type is not None:
            if not isinstance(filtre_type, list):
                raise TypeError("Le filtre type soit être une liste de str.")
            for type_ressource in filtre_type:
                if not isinstance(type_ressource, str):
                    raise TypeError("Le filtre type doit être une liste de str.")

        # filtre statut
        if filtre_statut is not None:
            if not isinstance(filtre_statut, list):
                raise TypeError("Le filtre statut doit être une liste de str.")
            for statut in filtre_statut:
                if not isinstance(statut, str):
                    raise TypeError("Le filtre statut doit être une liste de str.")

        # les dataframes
        if not isinstance(dataframe_ressources, DataFrameRessource):
            raise TypeError(
                "Le dataframe des ressources doit être de type DataFrameRessource."
            )
        if not isinstance(dataframe_casernes, DataFrameCaserne):
            raise TypeError(
                "Le dataframe des casernes doit être de type DataFrameCaserne."
            )

        # initialisation
        self.dataframe_ressources = dataframe_ressources
        self.dataframe_casernes = dataframe_casernes
        self.info_demandee = info_demandee
        self.filtre_pour_moyenne = filtre_pour_moyenne
        self.filtre_caserne = filtre_caserne
        self.filtre_type = filtre_type
        self.filtre_statut = filtre_statut

    def recuperer_liste(self) -> list[str]:
        """Méthode permettant de récupérer la liste des ressources concernées par les
        filtres.

        Returns:
            list[str]: La liste des ressources concernées par les filtres
        """
        ressources_concernees = []

        if self.filtre_type is not None:
            ressources_concernees_type = []
            for type_ressource in self.filtre_type:
                ressources_concernees_type.extend(
                    self.dataframe_ressources.df[
                        self.dataframe_ressources.df["type_ressource"] == type_ressource
                    ]["id_ressource"].to_list()
                )
            ressources_concernees.append(ressources_concernees_type)

        if self.filtre_statut is not None:
            ressources_concernees_statut = []
            for statut in self.filtre_statut:
                ressources_concernees_statut.extend(
                    self.dataframe_ressources.df[
                        self.dataframe_ressources.df["statut"] == statut
                    ]["id_ressource"].to_list()
                )
            ressources_concernees.append(ressources_concernees_statut)

        # On récupère la liste des casernes concernées pour les filtres ville et caserne

        if self.filtre_ville is not None:
            casernes_concernees = TableauDeBordCaserne(
                self.dataframe_casernes, "liste", filtre_ville=self.filtre_ville
            ).recuperer_liste()
        if self.filtre_caserne is not None:
            if self.filtre_ville is None:
                casernes_concernees = self.filtre_caserne
            else:
                casernes_concernees = list(
                    set(casernes_concernees) & set(self.filtre_caserne)
                )

        # on récupère ainsi une liste des ressources concernées par les filtres ville
        # et caserne
        if self.filtre_ville is not None or self.filtre_caserne is not None:
            ressources_concernees_ville_caserne = []
            for caserne in casernes_concernees:
                index_caserne = self.dataframe_casernes.df.index[
                    self.dataframe_casernes.df["id_caserne"] == caserne
                ][0]
                ressources_concernees_ville_caserne.extend(
                    self.dataframe_casernes.df.at[
                        index_caserne, "liste_ressources"
                    ].split(",")
                )
            ressources_concernees.append(ressources_concernees_ville_caserne)

        # On récupère la liste finale:
        ressources_concernees_finale = self.dataframe_ressources.df[
            "id_ressource"
        ].to_list()
        for element in ressources_concernees:
            ressources_concernees_finale = list(
                set(ressources_concernees_finale) & set(element)
            )

        return ressources_concernees_finale

    def compter(self) -> int:
        """Méthode pour compter le nombre de ressources

        Returns:
            int: le nombre de ressources
        """
        return len(self.recuperer_liste())

    def moyenne(self) -> float:
        """Méthode pour faire une moyenne selon le filtre pour la moyenne

        Returns:
            float: la moyenne
        """
        if self.filtre_pour_moyenne == "par ville":
            if self.filtre_ville is not None:
                denom = len(self.filtre_ville)
            else:
                denom = self.dataframe_casernes.df["ville"].nunique()
            moyenne = self.compter() / denom

        if self.filtre_pour_moyenne == "par caserne":
            if self.filtre_caserne is not None:
                denom = len(self.filtre_caserne)
            else:
                denom = len(self.dataframe_casernes.df)
            moyenne = self.compter() / denom

        return round(moyenne, 2)

    def titre_affichage(self) -> str:
        """Méthode pour générer le titre de l'affichage du tableau de bord

        Returns:
            str: le titre
        """
        if self.info_demandee == "moyenne":
            titre_affichage = (
                f"moyenne du nombre de ressources {self.filtre_pour_moyenne}: \n"
            )
        else:
            titre_affichage = f"{self.info_demandee} ressources: \n"

        if self.filtre_ville is not None:
            villes_str = ""
            for ville in self.filtre_ville:
                villes_str += ville + ", "
            villes_str = villes_str.rstrip(", ")
            titre_affichage += f"Dans les villes: {villes_str}\n"

        if self.filtre_caserne is not None:
            filtre_caserne_str = []
            for caserne in self.filtre_caserne:
                filtre_caserne_str.append(str(caserne))
            titre_affichage += f"Dans les casernes: {', '.join(filtre_caserne_str)}\n"

        if self.filtre_type is not None:
            titre_affichage += f"De type: {', '.join(self.filtre_type)}\n"

        if self.filtre_statut is not None:
            titre_affichage += (
                f"Statut de la ressource: {', '.join(self.filtre_statut)}\n"
            )
        return titre_affichage

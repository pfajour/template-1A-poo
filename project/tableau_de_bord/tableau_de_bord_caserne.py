"""Implémentation de la classe TableauDeBordCaserne.
"""

from ..data_frame.data_frame_caserne import DataFrameCaserne
from .tableau_de_bord import TableauDeBord


class TableauDeBordCaserne(TableauDeBord):
    """Classe TableauDeBordCaserne"""

    def __init__(
        self,
        dataframe: DataFrameCaserne,
        info_demandee: str,
        filtre_ville: list[str] = None,
    ) -> None:

        super().__init__(info_demandee, filtre_ville)

        # On vérifie le type du dataframe
        if not isinstance(dataframe, DataFrameCaserne):
            raise TypeError("Le dataframe doit être de type DataFrameCaserne.")

        # On initialise
        self.dataframe = dataframe

    def recuperer_liste(self) -> list[int]:
        """Méthode permettant de récupérer la liste des casernes concernées par les
        filtres.

        Returns:
            list[int]: liste des casernes concernées par les filtres
        """
        casernes_concernees = self.dataframe.df["id_caserne"].to_list()

        if self.filtre_ville is not None:
            casernes_concernees_ville = []
            for ville in self.filtre_ville:
                casernes_concernees_ville.extend(
                    self.dataframe.df[
                        (self.dataframe.df["ville"] == ville)
                        | (self.dataframe.df["zip_code"] == ville)
                    ]["id_caserne"].to_list()
                )
            casernes_concernees = casernes_concernees_ville

        return casernes_concernees

    def compter(self) -> int:
        """Méthode por compter le nombre de caserne dans la liste des casernes
        concernées

        Returns:
            int: Le nombre de casernes
        """
        return len(self.recuperer_liste())

    def moyenne(self) -> float:
        """Méthode pour calculer la moyenne du nombre de caserne par ville.

        Returns:
            float: la moyenne
        """
        if self.filtre_ville is not None:
            moyenne = round(self.compter() / len(self.filtre_ville), 2)
        else:
            denom = self.dataframe.df["ville"].nunique()
            moyenne = round(self.compter() / denom)
        return moyenne

    def titre_affichage(self) -> str:
        """Méthode pour créer le titre de l'affichage du tableau de bord.

        Returns:
            str: le titre
        """
        if self.info_demandee == "moyenne":
            titre_affichage = "moyenne du nombre de casernes par ville: \n"
        else:
            titre_affichage = f"{self.info_demandee} de casernes: \n"

        if self.filtre_ville is not None:
            villes_str = ""
            for ville in self.filtre_ville:
                villes_str += ville + ", "
            villes_str = villes_str.rstrip(", ")
            titre_affichage += f"Dans les villes: {villes_str}\n"

        return titre_affichage

"""implémentation de la classe DataFrameUrgence
"""

import datetime as dt

from .data_frame import DataFrame


class DataFrameUrgence(DataFrame):
    """Classe DataFrameUrgence héritée de la classe DataFrame."""

    def __init__(self, data: list[dict]) -> None:
        self._verif_init(data)

        # rajouter une conditions sur les clés des dictionnaires
        if not set(data[0].keys()) == set(
            [
                "id_urgence",
                "date",
                "latitude",
                "longitude",
                "ville",
                "zip_code",
                "nature_urgence",
                "id_auteur_appel",
                "auteur_appel_nom",
                "auteur_appel_prenom",
                "auteur_appel_num_tel",
                "liste_ressources",
                "etat_urgence",
            ]
        ):
            raise ValueError(
                "Les clés des dictionnaires doivent correspondres aux "
                "attributs de la classe Urgence."
            )

        for element in data:
            element["date"] = dt.datetime.strptime(element["date"], "%Y-%m-%d").date()
            element["zip_code"] = str(element["zip_code"])

        super().__init__(data)

"""Classe DataFrameCaserne
"""

from .data_frame import DataFrame


class DataFrameCaserne(DataFrame):
    """Classe hérité de DataFrame qui permet de créer la bdd des casernes et d'accéder
    aux infos sur les casernes.
    """

    def __init__(self, data: list[dict]) -> None:
        self._verif_init(data)

        # rajouter une condition: les clés doivent être les attributs de Caserne
        if not set(data[0].keys()) == set(
            [
                "id_caserne",
                "nom_caserne",
                "ville",
                "zip_code",
                "liste_ressources",
                "latitude",
                "longitude",
            ]
        ):
            raise ValueError(
                "Les clés des dictionnaires doivent correspondre aux attributs de la "
                "classe Caserne."
            )

        for element in data:
            element["zip_code"] = str(element["zip_code"])
            element["id_caserne"] = int(element["id_caserne"])

        super().__init__(data)

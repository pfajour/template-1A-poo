"""Implémentation de la classe DataFrame.
"""

import pandas as pd


class DataFrame:
    """Classe permettant de créer et d'utiliser des dataframes."""

    def __init__(self, data: list[dict]) -> None:
        self._verif_init(data)
        self.df = pd.DataFrame(data)

    def _verif_init(self, data: list[dict]) -> None:
        """méthode permettant de faire les vérifications nécessaire à
        l'initialisation."""
        if not isinstance(data, list):
            raise TypeError("data doit être une liste.")
        for ligne in data:
            if not isinstance(ligne, dict):
                raise TypeError("data doit être une liste de dictionnaires.")
        cles = set(data[0].keys())
        if not all(set(d.keys()) == cles for d in data[1:]):
            raise ValueError(
                "Les dictionnaires de data doivent tous avoir les mêmes clés."
            )

    def to_dict(self) -> list[dict]:
        """Méthode permettant de transformer le dataframe en une liste de
        dictionnaire, chaque ligne correspond à un dictionnaire et chaque
        colonne à une clé.

        Returns
        -------
        list[dict]: liste de dictionnaire
        """
        return self.df.to_dict(orient="records")

    @classmethod
    def from_csv(cls, file_path: str):
        """Méthode de classe qui permet de créer une instance de DataFrame à
        partir d'un fichier csv.

        Args:
            file_path (str): Le chemin vers le fichier csv.

        Returns:
            DataFrame: L'instance de la classe DataFrame créée.
        """
        if not isinstance(file_path, str):
            raise TypeError("Le file path doit être une chaîne de caractères.")
        data = pd.read_csv(file_path, index_col=None)
        return cls(data.to_dict(orient="records"))

    def save_csv(self, file_path: str):
        """Méthode qui permet de sauvgarder le DataFrame au format csv.

        Args:
            file_path (str): Chemin vers lequel on souhaite souvgarder le
                             DataFrame.
        """
        if not isinstance(file_path, str):
            raise TypeError("Le file path doit être une chaîne de caractères.")
        self.df.to_csv(file_path, index=False)

    def __str__(self) -> str:
        return str(print(self.df))

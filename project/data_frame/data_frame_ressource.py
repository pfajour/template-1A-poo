"""Implémentation de la classe DataFrameRessource
"""

from .data_frame import DataFrame


class DataFrameRessource(DataFrame):
    """Classe DataFrameRessource"""

    def __init__(self, data: list[dict]) -> None:
        self._verif_init(data)

        # rajouter une condition: les clés doivent être les attributs de Ressource
        if not set(["id_ressource", "type_ressource", "statut"]) == set(data[0].keys()):
            raise ValueError(
                "Les clés des dictionnaires doivent correspondrent aux attributs de la "
                "classe Ressource."
            )

        super().__init__(data)

"""implémentation de la classe Ressource
"""

from ..data_frame.data_frame_ressource import DataFrameRessource


class Ressource:
    """classe Ressource"""

    def __init__(self, id_ressource: str, dataframe: DataFrameRessource) -> None:
        # On vérifie le type des arguments
        if not isinstance(id_ressource, str):
            raise TypeError("L'id de la ressource doit être une chaîne de charactères.")
        if not isinstance(dataframe, DataFrameRessource):
            raise TypeError("Le dataframe doit être un DataFrameRessource.")

        # On vérifie que la ressource est bien dans le dataframe des ressources

        if id_ressource not in dataframe.df["id_ressource"].to_list():
            raise ValueError(
                f"Aucune ressource dans le dataframe des ressources n'est "
                f"rattachée à l'id {id_ressource}"
            )

        index_ressource = dataframe.df.index[
            dataframe.df["id_ressource"] == id_ressource
        ][0]

        # Initialisation
        self.id_ressource = id_ressource
        self.type_ressource = dataframe.df.at[index_ressource, "type_ressource"]
        self.statut = dataframe.df.at[index_ressource, "statut"]
        self.dataframe = dataframe

    def __str__(self):
        return (
            f"ressource {self.id_ressource} de type {self.type_ressource}, statut:"
            f" {self.statut}"
        )

    def modifier_statut(self, nouveau_statut: str) -> None:
        """Méthode permettant de modifier le statut de la ressource

        Args:
            nouveau_statut (str): le statut de la ressource après modification
        """
        if not isinstance(nouveau_statut, str):
            raise TypeError("Le nouveau statut doit être une chaîne de caractères.")
        if nouveau_statut not in ["operationnel", "en service", "hors-service"]:
            raise ValueError(
                "La ressource ne peut être que operationnelle, en service"
                " ou hors-service."
            )

        self.statut = nouveau_statut
        self.save_ressource()

    def save_ressource(self):
        """Méthode permettant d'enregistrer les changements apportés au statut de la
        ressource dans le dataframe correspondant.
        """
        index_ressource = self.dataframe.df.index[
            self.dataframe.df["id_ressource"] == self.id_ressource
        ][0]
        self.dataframe.df.loc[index_ressource, "statut"] = self.statut

    def __eq__(self, value: object) -> bool:
        if not isinstance(value, Ressource):
            raise TypeError(
                "On ne peut tester l'égalité qu'entre deux objets de la "
                "classe Ressource."
            )
        return (
            self.id_ressource == value.id_ressource
            and self.type_ressource == value.type_ressource
        )

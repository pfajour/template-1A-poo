"""Implémentation de la classe Caserne.
"""

from ..data_frame.data_frame_caserne import DataFrameCaserne
from ..ville.ville import Ville
from .ressource import DataFrameRessource, Ressource


class Caserne:
    """Classe permettant de représenter une caserne."""

    def __init__(
        self,
        id_caserne: int,
        dataframe_caserne: DataFrameCaserne,
        dataframe_ressources: DataFrameRessource,
    ) -> None:

        # Vérification du type des arguments
        if not isinstance(id_caserne, int):
            raise TypeError("L'id de la caserne doit être un entier.")
        if not isinstance(dataframe_caserne, DataFrameCaserne):
            raise TypeError("dataframe doit être une instance de DataFrameCaserne.")

        # On vérifie que l'id correspond à une caserne dans le dataframe
        if id_caserne not in dataframe_caserne.df["id_caserne"].tolist():
            raise ValueError(f"Aucune caserne n'est rattachée à l'id {id_caserne}")

        index_caserne = dataframe_caserne.df.index[
            dataframe_caserne.df["id_caserne"] == id_caserne
        ][0]

        # Initialisation
        self.id_caserne = id_caserne

        self.nom_caserne = dataframe_caserne.df.at[index_caserne, "nom_caserne"]

        latitude = float(dataframe_caserne.df.at[index_caserne, "latitude"])
        longitude = float(dataframe_caserne.df.at[index_caserne, "longitude"])
        self.emplacement_caserne = (latitude, longitude)

        self.ville = Ville(
            dataframe_caserne.df.at[index_caserne, "ville"],
            str(dataframe_caserne.df.at[index_caserne, "zip_code"]),
        )

        liste_id_ressources = dataframe_caserne.df.at[index_caserne, "liste_ressources"]
        liste_id_ressources = liste_id_ressources.split(",")
        self.liste_ressources = {}
        for id_ressource in liste_id_ressources:
            self.liste_ressources[id_ressource] = Ressource(
                id_ressource, dataframe_ressources
            )

        self.dataframe_caserne = dataframe_caserne
        self.dataframe_ressources = dataframe_ressources

    def __str__(self) -> str:
        resource_str = "\n".join(
            str(resource) for resource in self.liste_ressources.values()
        )
        return (
            f"{self.nom_caserne}, située à {self.ville}\n"
            f"liste de ressource:\n"
            f"{resource_str}"
        )

    def ceder_ressource(self, caserne2: "Caserne", ressource_a_ceder: str) -> None:
        """méthode permettant à une caserne de céder une ressource à une autre caserne.

        Args:
            caserne2 (Caserne): la caserne qui reçoit la ressource
            ressource_a_ceder (str): l'id de la ressource à céder
        """
        # Vérification du type des parametres
        if not isinstance(caserne2, Caserne):
            raise TypeError("Caserne2 doit être de type Caserne.")
        if not isinstance(ressource_a_ceder, str):
            raise TypeError("L'id de la ressource à céder doit être de type Ressource.")

        # On effectue l'opération
        self.retirer_ressource(ressource_a_ceder)
        caserne2.ajouter_ressource(ressource_a_ceder)

    def ajouter_ressource(self, ressource_a_ajouter: str) -> None:
        """méthode permettant d'ajouter une ressource à la liste des ressources de la
        caserne.

        Args:
            ressource_a_ajouter (str): l'id de la ressource à ajouter
        """
        # On vérifie le type des arguments
        if not isinstance(ressource_a_ajouter, str):
            raise TypeError("L'id de la ressource à ajouter doit être de type str.")

        # On vérifie que la ressource n'est pas déjà dans la caserne
        if ressource_a_ajouter in self.liste_ressources:
            raise ValueError(
                "La ressource à ajouter est déjà présente dans la caserne."
            )

        # On ajoute la ressource
        self.liste_ressources[ressource_a_ajouter] = Ressource(
            ressource_a_ajouter, self.dataframe_ressources
        )

        # On enregistre le changement
        self.save_caserne()

    def retirer_ressource(self, ressource_a_retirer: str) -> None:
        """méthode permettant d'ajouter une ressource à la liste des ressources de la
        caserne.

        Args:
            ressource_a_retirer (str): l'id de la ressource à ajouter
        """
        # On vérifie le type des arguments
        if not isinstance(ressource_a_retirer, str):
            raise TypeError("L'id de la ressource à retirer doit être de type str.")

        # On véifie que la caserne possède bien la ressource
        if ressource_a_retirer not in self.liste_ressources:
            raise ValueError(
                "La ressource à retirer n'est pas dans les ressources de la caserne."
            )

        # On retire la ressource
        self.liste_ressources.pop(ressource_a_retirer)

        # On enregistre le changement
        self.save_caserne()

    def changer_statut_ressource(self, id_ressource: str, nouveau_statut: str) -> None:
        """Méthode pour changer le statut d'une ressource"""
        self.liste_ressources[id_ressource].modifier_statut(nouveau_statut)
        self.save_caserne()

    def save_caserne(self):
        """Méthode pour sauvegarder les changements sur la base de données des casernes.

        Args:
            bdd (DataFrameCaserne): Base de données comprenant les infos sur les
                                    casernes.
        """
        index_caserne = self.dataframe_caserne.df.index[
            self.dataframe_caserne.df["id_caserne"] == self.id_caserne
        ][0]
        liste_ressoures_str = ",".join(element for element in self.liste_ressources)
        self.dataframe_caserne.df.loc[index_caserne, "liste_ressources"] = (
            liste_ressoures_str
        )

    @classmethod
    def creer_dict_caserne(
        cls,
        dataframe_caserne: "DataFrameCaserne",
        dataframe_ressources: DataFrameRessource,
    ) -> dict["Caserne"]:
        """Méthode de classe permettant de générer un dictionnaire de casernes depuis un
        dataframe.

        Args:
            dataframe_caserne (DataFrameCaserne): le dataframe à partir duquel on veut
                                                  importer les casernes.
            dataframe_ressoures (DataFrameRessource): le dataframe de ressources asocié

        Returns:
            dict[Caserne]: un dictionnaire de caserne. Les clés sont les id sous forme
                           d'int et les valeurs des objets de la classe Caserne.
        """
        # on vérifie le type des arguments
        if not isinstance(dataframe_caserne, DataFrameCaserne):
            raise TypeError(
                "Le dataframe des casernes doit être de type DataFrameCaserne."
            )
        if not isinstance(dataframe_ressources, DataFrameRessource):
            raise TypeError(
                "Le dataframe des ressources doit être de type DataFrameRessource."
            )

        # On crée le dictionnaire
        dict_casernes = {}

        for id_caserne in dataframe_caserne.df["id_caserne"].to_list():
            dict_casernes[id_caserne] = Caserne(
                id_caserne, dataframe_caserne, dataframe_ressources
            )

        return dict_casernes

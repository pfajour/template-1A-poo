"""Test pour la classe Ville.
"""

import re

import pytest

from ..project.ville.ville import Ville


@pytest.mark.parametrize(
    "nom, zip_code, message_erreur",
    [
        (
            ["Dawsonville"],
            "20837",
            "Le nom doit être une chaîne de caractères.",
        ),
        (
            20837,
            "20837",
            "Le nom doit être une chaîne de caractères.",
        ),
        (
            "Dawsonville",
            [20837],
            "Le zip code doit être une chaîne de caractères.",
        ),
        (
            "Dawsonville",
            20837,
            "Le zip code doit être une chaîne de caractères.",
        ),
    ],
)
def test_ville_init_erreur(nom: str, zip_code: int, message_erreur: str):
    """test des messages d'erreur de l'initialisation de la classe Ville."""
    with pytest.raises(TypeError, match=re.escape(message_erreur)):
        Ville(nom, zip_code)


@pytest.mark.parametrize(
    "nom, zip_code", [("Dawsonville", "20837"), ("Olney", "20832")]
)
def test_init_succes(nom: str, zip_code: str):
    """Test du succès de l'initiation de la classe Ville."""
    ville = Ville(nom, zip_code)
    assert (ville.nom == nom) and (ville.zip_code == zip_code)


def test_str_et_repr_succes():
    """Test du succès de str et repr pour la classe Ville."""
    ville = Ville("Dawsonville", "20837")
    assert (
        str(ville) == "Dawsonville : 20837"
        and repr(ville) == "Ville(Dawsonville, 20837)"
    )

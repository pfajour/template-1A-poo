"""Tests pour la classe Ressource
"""

import re

import pytest

from ..project.centre_secours.ressource import Ressource
from ..project.data_frame.data_frame_ressource import DataFrameRessource

dataframe_ressource = DataFrameRessource.from_csv("data/bdd_ressources.csv")


@pytest.mark.parametrize(
    "id_ressource, dataframe, erreur, message_erreur",
    [
        (
            1,
            dataframe_ressource,
            TypeError,
            "L'id de la ressource doit être une chaîne de charactères.",
        ),
        (
            ["T740"],
            dataframe_ressource,
            TypeError,
            "L'id de la ressource doit être une chaîne de charactères.",
        ),
        (
            "T740",
            "dataframe",
            TypeError,
            "Le dataframe doit être un DataFrameRessource.",
        ),
        (
            "T740",
            [dataframe_ressource],
            TypeError,
            "Le dataframe doit être un DataFrameRessource.",
        ),
        (
            "id_ressource",
            dataframe_ressource,
            ValueError,
            "Aucune ressource dans le dataframe des ressources n'est rattachée à l'id"
            " id_ressource",
        ),
    ],
)
def test_ressource_init_erreur(id_ressource, dataframe, erreur, message_erreur: str):
    """test des erreurs pour l'init de Ressource"""
    with pytest.raises(erreur, match=re.escape(message_erreur)):
        Ressource(id_ressource, dataframe)


def test_ressource_str_succes():
    """Test du succès de str pour Ressource.
    """
    ressource = Ressource("T740", dataframe_ressource)
    statut = ressource.statut
    assert str(ressource) == f"ressource T740 de type Truck, statut: {statut}"

""" Tests de la classe DataFrame
"""

import re

import pandas as pd
import pytest

from ..project.data_frame.data_frame import DataFrame


@pytest.mark.parametrize(
    "data, type_erreur, message_erreur",
    [
        (
            "[{'a': 1, 'b': 2}, {'a': 3, 'b': 4}]",
            TypeError,
            "data doit être une liste.",
        ),
        (
            [{"a": 1, "b": 2}, "{'a': 3, 'b': 4}"],
            TypeError,
            "data doit être une liste de dictionnaires.",
        ),
        (
            [{"a": 1, "b": 2}, {"a": 3, "c": 4}],
            ValueError,
            "Les dictionnaires de data doivent tous avoir les mêmes clés.",
        ),
    ],
)
def test_data_frame_init_erreur(data: list[dict], type_erreur, message_erreur: str):
    """test pour l'initialisation de la classe DataFrame. Messages d'erreur."""
    with pytest.raises(type_erreur, match=re.escape(message_erreur)):
        DataFrame(data)


@pytest.mark.parametrize(
    "data",
    [
        (
            [
                {"a": 1, "b": 2, "d": 3},
                {"a": 3, "b": 4, "d": 8},
                {"a": 54, "b": 12, "d": 9},
            ]
        ),
        (
            [
                {"f": 43, "g": 2, "h": 3},
                {"f": 4, "g": 127, "h": 8},
                {"f": 54, "g": 100, "h": 9},
            ]
        ),
    ],
)
def test_data_frame_init_succes(data: list[dict]):
    """test pour l'initialisation de la classe DataFrame. Succès."""
    assert DataFrame(data).df.equals(pd.DataFrame(data))

"""Module pour les tests de la classe Urgence.
"""

import datetime

import pytest

from ..project.centre_secours.ressource import Ressource
from ..project.data_frame.data_frame_caserne import DataFrameCaserne
from ..project.data_frame.data_frame_ressource import DataFrameRessource
from ..project.data_frame.data_frame_urgence import DataFrameUrgence
from ..project.declaration_urgence.urgence import Urgence


@pytest.mark.parametrize(
    "id_urgence, dataframe_urgences, dataframe_caserne, dataframe_ressource, date, "
    "emplacement, ville, zip_code, nature_urgence, auteur_appel_nom, "
    "auteur_appel_prenom, auteur_appel_num_tel, liste_ressources, "
    "liste_ressources_attendue",
    [
        (
            2,
            DataFrameUrgence(
                [
                    {
                        "id_urgence": 1,
                        "date": "2023-01-01",
                        "latitude": 39.147319,
                        "longitude": -77.135725,
                        "ville": "Redland",
                        "zip_code": "20855",
                        "nature_urgence": "une urgence",
                        "id_auteur_appel": 1,
                        "auteur_appel_nom": "nom",
                        "auteur_appel_prenom": "prenom",
                        "auteur_appel_num_tel": "000-000-000",
                        "liste_ressources": "PE728",
                        "etat_urgence": "finie",
                    }
                ]
            ),
            DataFrameCaserne(
                [
                    {
                        "id_caserne": 1,
                        "nom_caserne": "caserne proche",
                        "ville": "Olney",
                        "zip_code": "20832",
                        "liste_ressources": "Am1",
                        "latitude": 39.13404176,
                        "longitude": -77.06921915,
                    },
                    {
                        "id_caserne": 2,
                        "nom_caserne": "caserne loin",
                        "ville": "Rennes",
                        "zip_code": "35000",
                        "liste_ressources": "Am2",
                        "latitude": 48.124799,
                        "longitude": -1.713507,
                    },
                ]
            ),
            DataFrameRessource(
                [
                    {
                        "id_ressource": "Am1",
                        "type_ressource": "Ambulance",
                        "statut": "operationnel",
                    },
                    {
                        "id_ressource": "Am2",
                        "type_ressource": "Ambulance",
                        "statut": "operationnel",
                    },
                ]
            ),
            datetime.date(2024, 5, 1),
            (39.142551, -77.075362),
            "Olney",
            "20832",
            "une autre urgence",
            "nom2",
            "prenom2",
            "111-111-111",
            {"Ambulance": 1},
            [
                Ressource(
                    "Am1",
                    DataFrameRessource(
                        [
                            {
                                "id_ressource": "Am1",
                                "type_ressource": "Ambulance",
                                "statut": "operationnel",
                            },
                            {
                                "id_ressource": "Am2",
                                "type_ressource": "Ambulance",
                                "statut": "operationnel",
                            },
                        ]
                    ),
                )
            ],
        )
    ],
)
def test_urgence_algo_trouver_ressource(
    id_urgence: int,
    dataframe_urgences: DataFrameUrgence,
    dataframe_caserne: DataFrameCaserne,
    dataframe_ressource: DataFrameRessource,
    date: datetime.date,
    emplacement: tuple,
    ville: str,
    zip_code: str,
    nature_urgence: str,
    auteur_appel_nom: str,
    auteur_appel_prenom: str,
    auteur_appel_num_tel: str,
    liste_ressources: dict,
    liste_ressources_attendue: list[Ressource],
):
    """Test pour l'algo permettant de trouver les ressources lors de la création d'une
    urgence.
    """
    urgence = Urgence(
        id_urgence,
        dataframe_urgences,
        dataframe_caserne,
        dataframe_ressource,
        date,
        emplacement,
        ville,
        zip_code,
        nature_urgence,
        auteur_appel_nom,
        auteur_appel_prenom,
        auteur_appel_num_tel,
        liste_ressources,
    )
    assert urgence.liste_ressources == liste_ressources_attendue

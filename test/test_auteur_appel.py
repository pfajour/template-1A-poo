""" Tests de la classe AuteurAppel
"""

import re

import pytest

from ..project.declaration_urgence.auteur_appel import AuteurAppel


@pytest.mark.parametrize(
    "id_auteur_appel, nom, prenom, num_tel, message_erreur",
    [
        (
            [12345],
            "Smith",
            "John",
            "210-100-366",
            "L'identifiant doit être un entier.",
        ),
        (
            12345,
            ["Smith"],
            "John",
            "210-100-366",
            "Le nom doit être une chaîne de caractères.",
        ),
        (
            12345,
            "Smith",
            ["John"],
            "210-100-366",
            "Le prénom doit être une chaîne de caractères.",
        ),
        (
            12345,
            "Smith",
            "John",
            [210 - 100 - 366],
            "Le numéro de téléphone doit être une chaîne de caractères.",
        ),
        (
            12345,
            "Smith",
            "John",
            "210-100",
            "Le format du numéro n'est pas correct.",
        ),
        (
            12345,
            "Smith",
            "John",
            "210-1004-366",
            "Le format du numéro n'est pas correct.",
        ),
        (
            12345,
            "Smith",
            "John",
            "A10-100-366",
            "Le format du numéro n'est pas correct.",
        ),
    ],
)
def test_auteur_appel_init_erreur(
    id_auteur_appel: int, nom: str, prenom: str, num_tel: str, message_erreur: str
):
    """test des messages d'erreur de l'initialisation de la classe AuteurAppel."""
    with pytest.raises(TypeError, match=re.escape(message_erreur)):
        AuteurAppel(id_auteur_appel, nom, prenom, num_tel)


@pytest.mark.parametrize(
    "id_auteur_appel, nom, prenom, num_tel",
    [(12345, "Smith", "John", "210-100-366"), (67890, "Paul", "Paul", "123-123-123")],
)
def test_init_and_str_succes(id_auteur_appel: int, nom: str, prenom: str, num_tel: str):
    """test du succès de l'initialisation de la classe AuteurAppel."""
    auteur_appel = AuteurAppel(id_auteur_appel, nom, prenom, num_tel)
    assert (
        auteur_appel.id_auteur_appel == id_auteur_appel
        and auteur_appel.nom == nom
        and auteur_appel.prenom == prenom
        and auteur_appel.num_tel == num_tel
        and (
            str(auteur_appel) == f"{auteur_appel.id_auteur_appel}, {auteur_appel.nom}, "
            f"{auteur_appel.prenom}, {auteur_appel.num_tel}"
        )
    )


@pytest.mark.parametrize(
    "value2", [(12345), ("John Smith"), ([12345, "Smith", "John", "210-100-366"])]
)
def test_auteur_appel_eq_erreur(value2):
    """Test des erreurs de la fonction __eq__ pour la classe AuteurAppel"""
    with pytest.raises(
        TypeError,
        match=re.escape(
            "On ne peut tester l'égalité qu'entre"
            " deux objet de la classe"
            " AuteurAppel."
        ),
    ):
        assert AuteurAppel(1234, "Smith", "John", "210-100-366") == value2


@pytest.mark.parametrize(
    "auteur_appel2, result",
    [
        (AuteurAppel(1, "Smith", "John", "123-456-789"), True),
        (AuteurAppel(2, "Smith", "John", "123-456-789"), False),
    ],
)
def test_auteur_appel_eq_succes(auteur_appel2, result):
    """test du succès de la fonction eq"""
    auteur_appel1 = AuteurAppel(1, "Smith", "John", "123-456-789")
    eq_result = auteur_appel1 == auteur_appel2
    assert eq_result == result

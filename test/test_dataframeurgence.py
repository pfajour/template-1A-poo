""" Tests de la classe DataFrameUrgence
"""

import re

import pandas as pd
import pytest

from ..project.data_frame.data_frame_urgence import DataFrameUrgence


@pytest.mark.parametrize(
    "data, message_erreur",
    [
        (
            [
                {
                    "couleur_urgence": 12345,
                    "date": "03/02/2024",
                    "latitude": "45.5",
                    "longitude": "57.9",
                    "ville": "Redland",
                    "zip_code": "4157",
                    "nature_urgence": "incendie",
                    "id_auteur_appel": 4321,
                    "auteur_appel_nom": "Boup",
                    "auteur_appel_prenom": "Zorglub",
                    "auteur_appel_num_tel": "334-555-876",
                    "liste_ressources": ["A10", "A76"],
                    "etat_urgence": "en cours",
                },
                {
                    "couleur_urgence": 12885,
                    "date": "03/02/2024",
                    "latitude": "43.5",
                    "longitude": "87.9",
                    "ville": "Redland",
                    "zip_code": "4157",
                    "nature_urgence": "Python dans une maison",
                    "id_auteur_appel": 4321,
                    "auteur_appel_nom": "Boup",
                    "auteur_appel_prenom": "Zorgloub",
                    "auteur_appel_num_tel": "334-555-876",
                    "liste_ressources": ["A10", "A76"],
                    "etat_urgence": "en cours",
                },
            ],
            "Les clés des dictionnaires doivent correspondres aux attributs de la"
            " classe Urgence.",
        ),
    ],
)
def test_data_frame_urgence_init_erreur(data: list[dict], message_erreur: str):
    """test pour les erreurs de l'initialisation de la classe DataFrameUrgence."""
    with pytest.raises(ValueError, match=re.escape(message_erreur)):
        DataFrameUrgence(data)


@pytest.mark.parametrize(
    "data",
    [
        [
            {
                "id_urgence": 12345,
                "date": "2024-02-03",
                "latitude": "45.5",
                "longitude": "57.9",
                "ville": "Redland",
                "zip_code": "4157",
                "nature_urgence": "incendie",
                "id_auteur_appel": 4321,
                "auteur_appel_nom": "Boup",
                "auteur_appel_prenom": "Zorglub",
                "auteur_appel_num_tel": "334-555-876",
                "liste_ressources": ["A10", "A76"],
                "etat_urgence": "en cours",
            },
            {
                "id_urgence": 12345,
                "date": "2024-02-03",
                "latitude": "45.5",
                "longitude": "57.9",
                "ville": "Redland",
                "zip_code": "4157",
                "nature_urgence": "incendie",
                "id_auteur_appel": 4321,
                "auteur_appel_nom": "Boup",
                "auteur_appel_prenom": "Zorglub",
                "auteur_appel_num_tel": "334-555-876",
                "liste_ressources": ["A10", "A76"],
                "etat_urgence": "en cours",
            },
        ],
    ],
)
def test_data_frame_urgence_init_succes(data: list[dict]):
    """test pour l'initialisation de la classe DataFrameUrgence. Succès."""
    assert DataFrameUrgence(data).df.equals(pd.DataFrame(data))

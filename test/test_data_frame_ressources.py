""" Tests de la classe DataFrameRessource
"""

import re

import pandas as pd
import pytest

from ..project.data_frame.data_frame_ressource import DataFrameRessource


@pytest.mark.parametrize(
    "data, message_erreur",
    [
        (
            [
                {
                    "prix_ressource": 12345,
                    "type_ressource": "camion",
                    "statut": "hors service",
                },
                {
                    "prix_ressource": 12345,
                    "type_ressource": "camion",
                    "statut": "hors service",
                },
            ],
            "Les clés des dictionnaires doivent correspondrent aux attributs de la "
            "classe Ressource.",
        ),
    ],
)
def test_data_frame_ressource_init_erreur(data: list[dict], message_erreur: str):
    """test pour les erreurs de l'initialisation de la classe DataFrameUrgence."""
    with pytest.raises(ValueError, match=re.escape(message_erreur)):
        DataFrameRessource(data)


@pytest.mark.parametrize(
    "data",
    [
        [
            {
                "id_ressource": 12345,
                "type_ressource": "camion",
                "statut": "hors service",
            },
            {
                "id_ressource": 12345,
                "type_ressource": "camion",
                "statut": "hors service",
            },
        ],
    ],
)
def test_data_frame_ressource_init_succes(data: list[dict]):
    """test pour l'initialisation de la classe DataFrameRessource. Succès."""
    assert DataFrameRessource(data).df.equals(pd.DataFrame(data))

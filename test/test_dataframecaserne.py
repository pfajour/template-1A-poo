""" Tests de la classe DataFrameCaserne
"""

import re

import pandas as pd
import pytest

from ..project.data_frame.data_frame_caserne import DataFrameCaserne


@pytest.mark.parametrize(
    "data, message_erreur",
    [
        (
            [
                {
                    "CouleurCaserne": 12345,
                    "nom_caserne": "Caserne",
                    "ville": "Redland",
                    "zip_code": "654321",
                    "liste_ressources": ["A20", "A22"],
                    "latitude": "50.0",
                    "longitude": "60.5",
                },
                {
                    "CouleurCaserne": 12345,
                    "nom_caserne": "Caserne",
                    "ville": "Redland",
                    "zip_code": "654321",
                    "liste_ressources": ["A20", "A22"],
                    "latitude": "50.0",
                    "longitude": "60.5",
                },
            ],
            "Les clés des dictionnaires doivent correspondre aux attributs"
            " de la classe Caserne.",
        ),
    ],
)
def test_data_frame_init_erreur(data: list[dict], message_erreur: str):
    """test pour les erreurs de l'initialisation de la classe DataFrameCaserne."""
    with pytest.raises(ValueError, match=re.escape(message_erreur)):
        DataFrameCaserne(data)


@pytest.mark.parametrize(
    "data",
    [
        [
            {
                "id_caserne": 12345,
                "nom_caserne": "Caserne",
                "ville": "Redland",
                "zip_code": "654321",
                "liste_ressources": ["A20", "A22"],
                "latitude": "50.0",
                "longitude": "60.5",
            },
            {
                "id_caserne": 12345,
                "nom_caserne": "Caserne",
                "ville": "Redland",
                "zip_code": "654321",
                "liste_ressources": ["A20", "A22"],
                "latitude": "50.0",
                "longitude": "60.5",
            },
        ],
    ],
)
def test_data_frame_caserne_init_succes(data: list[dict]):
    """test pour l'initialisation de la classe DataFrameCaserne. Succès."""
    assert DataFrameCaserne(data).df.equals(pd.DataFrame(data))
